# Adtran Mosaic Cloud Platform

Vendor: Adtran
Homepage: https://www.adtran.com/en

Product: Adtran Mosaic Cloud Platform
Product Page: https://www.adtran.com/en/products-and-services/cloud-software/management-and-orchestration/mosaic-cloud-platform

## Introduction
We classify Adtran Mosaic Cloud Platform into the Cloud domain as Adtran Mosaic Cloud Platform provides Cloud Services. We also classify it into the Inventory domain because it contains an inventory of Adtran Devices.

"Adtran Mosaic Cloud Platform increases profitability by supporting orchestration across multiple vendor systems in multiple domains" 

The Adtran Mosaic Cloud Platform adapter can be integrated to the Itential Device Broker which will allow your Adtran Devices to be managed within the Itential Configuration Manager Application. 

## Why Integrate
The Adtran Mosaic Cloud Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Adtran Mosaic Cloud Platform. With this adapter you have the ability to perform operations such as:

- Configuring Devices

## Additional Product Documentation
The [API documents for Adtran Mosaic Cloud Platform](https://documenter.getpostman.com/view/2389999/TVsrGVaa)
