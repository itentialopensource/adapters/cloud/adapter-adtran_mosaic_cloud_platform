# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Adtran_mosaic_cloud_platform System. The API that was used to build the adapter for Adtran_mosaic_cloud_platform is usually available in the report directory of this adapter. The adapter utilizes the Adtran_mosaic_cloud_platform API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Adtran Mosaic Cloud Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Adtran Mosaic Cloud Platform. With this adapter you have the ability to perform operations such as:

- Configuring Devices

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
