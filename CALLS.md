## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Adtran Mosaic Cloud Platform. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Adtran Mosaic Cloud Platform.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adtran_mosaic_cloud_platform. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">requestaSessionToken(body, callback)</td>
    <td style="padding:15px">Request a Session Token</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-token:request-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseaSessionToken(body, callback)</td>
    <td style="padding:15px">Release a Session Token</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-token:release-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyingPasswordSecuritySetting(body, callback)</td>
    <td style="padding:15px">Modifying Password Security Setting</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config:aaa-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievePasswordSecuritySetting(callback)</td>
    <td style="padding:15px">Retrieve Password Security Setting</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config:aaa-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(group, body, callback)</td>
    <td style="padding:15px">Create Group</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-group:groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(group, callback)</td>
    <td style="padding:15px">Delete Group</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-group:groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroup(group, callback)</td>
    <td style="padding:15px">Get Group</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-group:groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGroups(callback)</td>
    <td style="padding:15px">Get All Groups</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-group:groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveModulePermissions(callback)</td>
    <td style="padding:15px">Retrieve Module Permissions</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-permission:permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserAccount(body, callback)</td>
    <td style="padding:15px">Create User Account</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:create-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overridePassword(body, callback)</td>
    <td style="padding:15px">Override Password</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:override-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPassword(body, callback)</td>
    <td style="padding:15px">Reset Password</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:reset-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignUsertoUserGroup(user, body, callback)</td>
    <td style="padding:15px">Assign User to User Group</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-user:users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(user, callback)</td>
    <td style="padding:15px">Get User</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-user:users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyUserAAAModes(user, body, callback)</td>
    <td style="padding:15px">Modify User - AAA Modes</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-user:users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsers(callback)</td>
    <td style="padding:15px">Get All Users</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-user:users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPassword(username, body, callback)</td>
    <td style="padding:15px">Set Password</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:set-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutUser(body, callback)</td>
    <td style="padding:15px">Logout User</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:logout-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expirePassword(body, callback)</td>
    <td style="padding:15px">Expire Password</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:expire-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutAllUsers(callback)</td>
    <td style="padding:15px">Logout All Users</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:logout-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(body, callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-user:delete-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVendorSpecificAttributeMappingforRadius(body, callback)</td>
    <td style="padding:15px">Create Vendor Specific Attribute Mapping for Radius</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-radius-attributes-mapping:radius-attributes/vsa-to-permission-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorSpecificAttributeMappingforRadius(callback)</td>
    <td style="padding:15px">Get Vendor Specific Attribute Mapping for Radius</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-radius-attributes-mapping:radius-attributes/vsa-to-permission-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVendorSpecificAttributeMappingforRadius(group, body, callback)</td>
    <td style="padding:15px">Modify Vendor Specific Attribute Mapping for Radius</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-radius-attributes-mapping:radius-attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendorSpecificAttributeMappingforRadius(group, callback)</td>
    <td style="padding:15px">Delete Vendor Specific Attribute Mapping for Radius</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-radius-attributes-mapping:radius-attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountLogsBetweenDatesandTime(body, callback)</td>
    <td style="padding:15px">Get Account Logs Between Dates and Time</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-auth-accounting:get-filtered-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCredential(body, callback)</td>
    <td style="padding:15px">Create Credential</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials/credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCredentialPasswordOnlyAuthentication(credential, body, callback)</td>
    <td style="padding:15px">Update Credential Password Only Authentication</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialPasswordOnly(callback)</td>
    <td style="padding:15px">Delete Credential Password Only</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialsByName(callback)</td>
    <td style="padding:15px">Get Credentials By Name</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredential(callback)</td>
    <td style="padding:15px">Delete Credential</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCredentials(callback)</td>
    <td style="padding:15px">Get All Credentials</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-credentials:credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customizeCommand(body, callback)</td>
    <td style="padding:15px">Customize Command</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-ui-inspect:request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExternalPolicyRADIUS(body, callback)</td>
    <td style="padding:15px">Create External Policy - RADIUS</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/auth-method?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalPolicyRADIUS(authMethod, body, callback)</td>
    <td style="padding:15px">Update External Policy - RADIUS</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalPolicyRADIUS(authMethod, callback)</td>
    <td style="padding:15px">Delete External Policy - RADIUS</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalPolicyTACACS(authMethod, body, callback)</td>
    <td style="padding:15px">Update External Policy - TACACS+</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalPolicyTACACS(authMethod, callback)</td>
    <td style="padding:15px">Delete External Policy - TACACS+</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalPolicy(authMethod, callback)</td>
    <td style="padding:15px">Get External Policy</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-auth-config-external:auth-methods/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLoginBanner(body, callback)</td>
    <td style="padding:15px">Update Login Banner</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-launchpad-login:login-screen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoginBanner(callback)</td>
    <td style="padding:15px">Get Login Banner</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-launchpad-login:login-screen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleRollover(body, callback)</td>
    <td style="padding:15px">Schedule Rollover</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-elasticsearch-rollover-indices:schedule-rollover-attempt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRolloverIndices(callback)</td>
    <td style="padding:15px">Get All Rollover Indices</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-elasticsearch-rollover-indices:indices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportInterfaceInventory(body, callback)</td>
    <td style="padding:15px">Export Interface Inventory</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-export-data-service:export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setInactivityLogoffTime(body, callback)</td>
    <td style="padding:15px">Set Inactivity Logoff Time</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-settings:settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInactivityAutologoffTime(callback)</td>
    <td style="padding:15px">Get Inactivity Autologoff Time</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-settings:settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleSignOnOIDCProvider(callback)</td>
    <td style="padding:15px">Get Single Sign On OIDC Provider</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-launchpad-login:login-screen/oidc-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSingleSignOnOIDCProvider(body, callback)</td>
    <td style="padding:15px">Set Single Sign On OIDC Provider</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-launchpad-login:login-screen/oidc-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearSingleSignOnOIDCProvider(oidcProvider, callback)</td>
    <td style="padding:15px">Clear Single Sign On OIDC Provider</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-launchpad-login:login-screen/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEvents(body, callback)</td>
    <td style="padding:15px">Get All Events</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-event-workflow:get-events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopic(body, callback)</td>
    <td style="padding:15px">Create Topic</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-topics:create-topic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopic(body, callback)</td>
    <td style="padding:15px">Update Topic</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-topics:update-topic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventTypes(callback)</td>
    <td style="padding:15px">Get Event Types</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-topics:event-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCreatedTopics(callback)</td>
    <td style="padding:15px">Get All Created Topics</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-topics:topics/topic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sNMPv2cSpecificFilter(body, callback)</td>
    <td style="padding:15px">SNMP v2c - Specific Filter</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAllTrapHosts(callback)</td>
    <td style="padding:15px">Retrieve All Trap Hosts</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveSpecificTrapHost(trapHost, callback)</td>
    <td style="padding:15px">Retrieve Specific Trap Host</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTrapHost(trapHost, body, callback)</td>
    <td style="padding:15px">Modify Trap Host</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrapHost(trapHost, callback)</td>
    <td style="padding:15px">Delete Trap Host</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveGlobalFilter(callback)</td>
    <td style="padding:15px">Retrieve Global Filter</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/global-filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyGlobalFilter(body, callback)</td>
    <td style="padding:15px">Modify Global Filter</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/global-filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveTrapForwarderState(callback)</td>
    <td style="padding:15px">Retrieve Trap Forwarder State</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-trap-forwarder:trap-forwarder/trap-forwarder-state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestEvent(latestEvent, callback)</td>
    <td style="padding:15px">Get Latest Event</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-latest-events:latest-events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLatestEvent(body, callback)</td>
    <td style="padding:15px">Delete Latest Event</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-latest-events:delete-events-entry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJobBackupDeviceConfiguration(body, callback)</td>
    <td style="padding:15px">Create Job- Backup Device Configuration</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployJobBackupDeviceConfiguration(body, callback)</td>
    <td style="padding:15px">Deploy Job- Backup Device Configuration</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateJobBackupDeviceConfiguration(body, callback)</td>
    <td style="padding:15px">Activate Job- Backup Device Configuration</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runJobBackupDeviceConfiguration(body, callback)</td>
    <td style="padding:15px">Run Job- Backup Device Configuration</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow-jobs:run-job-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runJobDownloadJob(body, callback)</td>
    <td style="padding:15px">Run Job - Download Job</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow-jobs:run-job-now-with-parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserInitiatedPendingJobsSize(callback)</td>
    <td style="padding:15px">Get User Initiated Pending Jobs Size</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/user-initiated/size?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserInitiatedPendingJobsList(callback)</td>
    <td style="padding:15px">Get User Initiated Pending Jobs List</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/user-initiated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserInitiatedPendingJobs(callback)</td>
    <td style="padding:15px">Get User Initiated Pending Jobs</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/user-initiated/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserInitiatedPendingJob(job, callback)</td>
    <td style="padding:15px">Delete User Initiated Pending Job</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/user-initiated/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpawnedPendingJobsSize(callback)</td>
    <td style="padding:15px">Get Spawned Pending Jobs Size</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/spawned/size?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpawnedPendingJobsList(callback)</td>
    <td style="padding:15px">Get Spawned Pending Jobs List</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/spawned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpawnedPendingJobs(callback)</td>
    <td style="padding:15px">Get Spawned Pending Jobs</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:pending-jobs/spawned/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecutingJobsSize(callback)</td>
    <td style="padding:15px">Get Executing Jobs Size</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:executing-jobs/size?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecutingJobsList(callback)</td>
    <td style="padding:15px">Get Executing Jobs List</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:executing-jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecutingJobs(callback)</td>
    <td style="padding:15px">Get Executing Jobs</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-job-manager:executing-jobs/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobByName(job, callback)</td>
    <td style="padding:15px">Get Job By Name</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-jobs:jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllJobs(callback)</td>
    <td style="padding:15px">Get All Jobs</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-jobs:jobs/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllJobActions(callback)</td>
    <td style="padding:15px">Get All Job Actions</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-jobs:actions/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobContext(body, callback)</td>
    <td style="padding:15px">Get Job Context</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow-jobs:get-job-context?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobTriggers(callback)</td>
    <td style="padding:15px">Get Job Triggers</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-jobs:triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlatformInformation(callback)</td>
    <td style="padding:15px">Get Platform Information</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-information:information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginDetails(callback)</td>
    <td style="padding:15px">Get Plugin Details</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-plugin:plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllModelNamesinMosaicCP(callback)</td>
    <td style="padding:15px">Get All Model Names in Mosaic CP</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-part-numbers:part-numbers/part-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureFlag(callback)</td>
    <td style="padding:15px">Get Feature Flag</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-feature-flags:feature-flags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForceActions(callback)</td>
    <td style="padding:15px">Get Force Actions</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-force-actions:force-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUICapabilities(callback)</td>
    <td style="padding:15px">Get UI Capabilities</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-ui-capabilities:ui-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">platformStatusCheck(callback)</td>
    <td style="padding:15px">Platform Status Check</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-status:check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesSummaryusingSystemMonitor(callback)</td>
    <td style="padding:15px">Get Nodes Summary using System Monitor</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-system-monitor:nodes-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesusingSystemMonitor(callback)</td>
    <td style="padding:15px">Get Nodes using System Monitor</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-system-monitor:nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedObjectCreateBundle(body, callback)</td>
    <td style="padding:15px">Managed Object - Create Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-orchestration:create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedObjectModifyBundleOperationReplacewithRequiredParameters(body, callback)</td>
    <td style="padding:15px">Managed Object - Modify Bundle Operation Replace with Required Parameters</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-orchestration:modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedObjectDeleteBundle(body, callback)</td>
    <td style="padding:15px">Managed Object - Delete Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-orchestration:delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureBundle(body, callback)</td>
    <td style="padding:15px">Configure Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:configure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundle(bundle, callback)</td>
    <td style="padding:15px">Get Bundle</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-bundles:bundles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBundles(callback)</td>
    <td style="padding:15px">Get All Bundles</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-bundles:bundles/bundle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateBundle(body, callback)</td>
    <td style="padding:15px">Deactivate Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undeployBundle(body, callback)</td>
    <td style="padding:15px">Undeploy Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:undeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBundle(body, callback)</td>
    <td style="padding:15px">Delete Bundle</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContentProvider(contentProvider, callback)</td>
    <td style="padding:15px">Get Content Provider</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-services:content-providers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllContentProviders(callback)</td>
    <td style="padding:15px">Get All Content Providers</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-services:content-providers/content-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataCenter(dataCenter, callback)</td>
    <td style="padding:15px">Get Data Center</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-data-centers:data-centers/${pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDataCenters(callback)</td>
    <td style="padding:15px">Get All Data Centers</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-data-centers:data-centers/data-center?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDataCenterTypes(callback)</td>
    <td style="padding:15px">Get All Data Center Types</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-data-center-types:data-center-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceTypes(callback)</td>
    <td style="padding:15px">Get All Device Types</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-device-types:device-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdtranDevice(device, callback)</td>
    <td style="padding:15px">Get Device</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-devices:devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesusingQueryParams(fields, callback)</td>
    <td style="padding:15px">Get Devices using Query Params</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-devices:devices/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceRemoveDevice(body, callback)</td>
    <td style="padding:15px">Force Remove Device</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:force-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCompatibleModelNames(body, callback)</td>
    <td style="padding:15px">Get All Compatible Model Names</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:retrieve-compatible-model-names?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterface(interfaceName, callback)</td>
    <td style="padding:15px">Get Interface</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-interfaces:interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInterfaces(callback)</td>
    <td style="padding:15px">Get All Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-interfaces:interfaces/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInterfaceTypes(callback)</td>
    <td style="padding:15px">Get All Interface Types</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-interfaces:interface-types/interface-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryInterface(testUuid, callback)</td>
    <td style="padding:15px">Query Interface</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-interfaces:interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryInterfaceInspect(testUuid, callback)</td>
    <td style="padding:15px">Query Interface Inspect</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-interfaces:interfaces/{pathv1}/inspect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryInterfaceInspectattachedGadgets(testUuid, callback)</td>
    <td style="padding:15px">Query Interface Inspect attached-gadgets</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-interfaces:interfaces/{pathv1}/inspect/attached-gadgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDevice(testUuid, callback)</td>
    <td style="padding:15px">Query Device</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-devices:devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDeviceInspect(testUuid, callback)</td>
    <td style="padding:15px">Query Device Inspect</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-devices:devices/{pathv1}/inspect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDeviceInspectGadgetInfo(testUuid, callback)</td>
    <td style="padding:15px">Query Device Inspect Gadget-info</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-devices:devices/{pathv1}/inspect/gadget-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryDeviceInspectGadgetUsage(testUuid, callback)</td>
    <td style="padding:15px">Query Device Inspect Gadget-usage</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-devices:devices/{pathv1}/inspect/gadget-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementDomain(managementDomain, callback)</td>
    <td style="padding:15px">Get Management Domain</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-management-domains:management-domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllManagementDomain(callback)</td>
    <td style="padding:15px">Get All Management Domain</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-management-domains:management-domains/management-domain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllManagementDomainTypes(callback)</td>
    <td style="padding:15px">Get All Management Domain Types</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-management-domains:management-domain-types/management-domain-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServer(server, callback)</td>
    <td style="padding:15px">Get Server</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-servers:servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllServers(callback)</td>
    <td style="padding:15px">Get All Servers</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-servers:servers/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findServices(body, callback)</td>
    <td style="padding:15px">Find Services</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-captive-portal-services:find-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendService(body, callback)</td>
    <td style="padding:15px">Suspend Service</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeService(body, callback)</td>
    <td style="padding:15px">Resume Service</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getService(service, callback)</td>
    <td style="padding:15px">Get Service</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-services:services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceProfiles(callback)</td>
    <td style="padding:15px">Get Service Profiles</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-services:service-profiles/service-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(callback)</td>
    <td style="padding:15px">Get Services</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-services:services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resolveProfilesforDevice(body, callback)</td>
    <td style="padding:15px">Resolve Profiles for Device</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:resolve-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProfiles(callback)</td>
    <td style="padding:15px">Get All Profiles</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addaProfile(body, callback)</td>
    <td style="padding:15px">Add a Profile</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfile(profile, callback)</td>
    <td style="padding:15px">Get Profile</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateaProfile(profile, body, callback)</td>
    <td style="padding:15px">Update a Profile</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeaProfile(profile, callback)</td>
    <td style="padding:15px">Remove a Profile</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverriddenProfilesListforAllMCPObjects(callback)</td>
    <td style="padding:15px">Get Overridden Profiles List for All MCP Objects</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-overrides:overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProfileVectors(callback)</td>
    <td style="padding:15px">Get All Profile Vectors</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addaProfileVector(vector, body, callback)</td>
    <td style="padding:15px">Add a Profile Vector</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileVector(vector, callback)</td>
    <td style="padding:15px">Get Profile Vector</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateaProfileVector(vector, body, callback)</td>
    <td style="padding:15px">Update a Profile Vector</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeaProfileVector(vector, callback)</td>
    <td style="padding:15px">Remove a Profile Vector</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConditionalProfileDeviceCondition(profile, callback)</td>
    <td style="padding:15px">Delete Conditional Profile (Device Condition)</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConditionalProfileInterfaceCondition(profile, callback)</td>
    <td style="padding:15px">Delete Conditional Profile (Interface Condition)</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectByInterfaceName(body, callback)</td>
    <td style="padding:15px">Select By Interface Name</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:select-behavior?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deselectInterfaceName(body, callback)</td>
    <td style="padding:15px">Deselect Interface Name</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:deselect-behavior?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupingProfileBehaviorGroup(profile, callback)</td>
    <td style="padding:15px">Delete Grouping Profile (Behavior Group)</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBehaviorGroups(callback)</td>
    <td style="padding:15px">Get All Behavior Groups</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-behaviors:behaviors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLabel(body, callback)</td>
    <td style="padding:15px">Add Label</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-labels:labels/label?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterForLabel(body, callback)</td>
    <td style="padding:15px">Data Filter For Label</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-labels:labels/label?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchLabel(body, callback)</td>
    <td style="padding:15px">Search Label</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-search:search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLabel(label, callback)</td>
    <td style="padding:15px">Delete Label</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-labels:labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">onboardDeviceUsingPortPattern(body, callback)</td>
    <td style="padding:15px">Onboard Device Using Port Pattern</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-uiworkflow:onboard-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkTransitions(transId, callback)</td>
    <td style="padding:15px">Check Transitions</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow:transitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaProfileVector(vector, callback)</td>
    <td style="padding:15px">Get a Profile Vector</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlltheProfileVectors(callback)</td>
    <td style="padding:15px">Get All the Profile Vectors</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-uiworkflow-profiles:profiles/vectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryTree(body, callback)</td>
    <td style="padding:15px">Get Inventory Tree</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-mcp-inventory-status:get-inventory-tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryStatus(body, callback)</td>
    <td style="padding:15px">Get Inventory Status</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-mcp-inventory-status:get-inventory-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInventoryStatusResult(body, callback)</td>
    <td style="padding:15px">Get Inventory Status Result</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-mcp-inventory-status:get-inventory-status-result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceProfileVectorForOnboarding(vector, body, callback)</td>
    <td style="padding:15px">Create Device Profile Vector For Onboarding</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterfaceProfileVectorForOnboarding(vector, body, callback)</td>
    <td style="padding:15px">Create Interface Profile Vector For Onboarding</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBundleProfileVectorForOnboarding(vector, body, callback)</td>
    <td style="padding:15px">Create Bundle Profile Vector For Onboarding</td>
    <td style="padding:15px">{base_path}/{version}/data/adtran-cloud-platform-profiles:profiles/vectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">componentAPI(body, callback)</td>
    <td style="padding:15px">Component API</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-device-onboarding:get-onboarding-template-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">onboardingAPI(body, callback)</td>
    <td style="padding:15px">Onboarding API</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-device-onboarding:onboard-device-by-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statusAPI(body, callback)</td>
    <td style="padding:15px">Status API</td>
    <td style="padding:15px">{base_path}/{version}/operations/adtran-cloud-platform-device-onboarding:get-onboarding-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterByPayloadBundle(body, callback)</td>
    <td style="padding:15px">Data Filter By Payload - Bundle</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-bundles:bundles/bundle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterByPayloadInterface(body, callback)</td>
    <td style="padding:15px">Data Filter By Payload - Interface</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-uiworkflow-interfaces:interfaces/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterByPayloadDevice(body, callback)</td>
    <td style="padding:15px">Data Filter By Payload - Device</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-devices:devices/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterContainsoperator(body, callback)</td>
    <td style="padding:15px">Data Filter - contains operator</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-interfaces:interfaces/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterLikeoperator(body, callback)</td>
    <td style="padding:15px">Data Filter - like operator</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-profiles:profiles/vectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterJoinoperator(body, callback)</td>
    <td style="padding:15px">Data Filter - join operator</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-interfaces:interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterDepthFirstoperator(singleActionSvcIntName, body, callback)</td>
    <td style="padding:15px">Data Filter - depth-first operator</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-interfaces:interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dataFilterCountoperator(device, body, callback)</td>
    <td style="padding:15px">Data Filter - count operator</td>
    <td style="padding:15px">{base_path}/{version}/data-filter/adtran-cloud-platform-devices:devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
