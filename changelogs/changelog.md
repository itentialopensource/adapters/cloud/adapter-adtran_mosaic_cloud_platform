
## 0.1.2 [11-03-2022]

* Add a new task and update existing tasks

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!1

---

## 0.1.1 [06-30-2022]

* Bug fixes and performance improvements

See commit ad75fcd

---
