
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:02PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_cloud_platform!18

---

## 0.4.4 [08-21-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-adtran_mosaic_cloud_platform!16

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:10PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_cloud_platform!15

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:23PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_cloud_platform!14

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_14:46PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_cloud_platform!13

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!12

---

## 0.3.6 [03-28-2024]

* Changes made at 2024.03.28_13:13PM

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!11

---

## 0.3.5 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!10

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.11_15:30PM

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!9

---

## 0.3.3 [02-28-2024]

* Changes made at 2024.02.28_11:41AM

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!8

---

## 0.3.2 [12-26-2023]

* update metadata

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!7

---

## 0.3.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!6

---

## 0.3.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!4

---

## 0.2.0 [11-13-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!4

---

## 0.1.2 [11-03-2022]

* Add a new task and update existing tasks

See merge request itentialopensource/adapters/cloud/adapter-adtran_mosaic_cloud_platform!1

---

## 0.1.1 [06-30-2022]

* Bug fixes and performance improvements

See commit ad75fcd

---
